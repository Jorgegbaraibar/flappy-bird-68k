; =============================================================================
; TITLE    : FLAPPY BIRD 68K
; AUTHOR   : MICHAEL GARCIA AND JORGE GOMEZ
; CREATION : 
; =============================================================================

            ORG     $1000

; --- CODE INCLUDES -----------------------------------------------------------

            INCLUDE "SYSCONST.X68"          ; SYSTEM CONSTANTS
            INCLUDE "SYSTEM.X68"            ; SYSTEM CODE
            INCLUDE "CONST.X68"             ; USER CONSTANTS
            INCLUDE "UTIL.X68"              ; UTILITY MACRO AND SUBROUTINES
            INCLUDE "PLAYER.X68"            ; PLAYER MANAGEMENT
            INCLUDE "AGENTLST.X68"          ; AGENT LIST MANAGEMENT
            INCLUDE "STATES.X68"            ; GAME STATES MANAGEMENT
            INCLUDE "HARDWARE.X68"          ; HARDWARE WINDOW SYSTEM
            INCLUDE "BALLSPWN.X68"          ; BALL SPAWNER
            INCLUDE "ASTEROID.X68"          ; BALL MANAGEMENT
            INCLUDE "SPAWNER.X68"           ; SPAWNER OF WALLS AND FLOORS
            INCLUDE "WALL.X68"              ; WALL MANAGEMENT
            INCLUDE "FLOOR.X68"             ; FLOOR MANAGEMENT
            INCLUDE "BOLLA.X68"             ; FLOOR MANAGEMENT
; --- INITIALIZE --------------------------------------------------------------

START       JSR     SYSINIT
            JSR     STAINIT                 ; INITIALIZE GAME STATE
            JSR     SHOWHWIN                ; SHOW HARDWARE WINDOW
           

; --- UPDATE ------------------------------------------------------------------

.LOOP       MOVE.B  #31,D0                  ; NUM OF CYCLES STORED IN VARIABLE
	        TRAP    #15
	        MOVE.L  D1, NUMCUPDT
            TRAP    #KBDTRAP                ; READ KEYBOARD
            JSR     STAUPD                  ; UPDATE DEPENDING ON THE STATE
            JSR     SVNACC                  ; CYCLE MANAGER
            MOVE.B  #30,D0		            ; CYCLE COUNTER CLEAR
	        TRAP    #15
            

; --- WAIT SYNCH --------------------------------------------------------------

.WINT       TST.B   (SCRINTCT)              ; WAIT FOR INTERRUPT
            BEQ     .WINT
            CLR.B   (SCRINTCT)

; --- PLOT --------------------------------------------------------------------
            MOVE.B  #31,D0		            ; NUM OF CYCLES STORED IN VARIABLE
	        TRAP    #15
	        MOVE.L  D1, NUMCPLOT
            JSR     STAPLOT                 ; PLOT DEPENDING ON THE STATE
            TRAP    #SCRTRAP                ; SHOW GRAPHICS
            JSR     SVNACC                  ; CYCLE MANAGER
            MOVE.B  #30,D0		            ; CYCLE COUNTER CLEAR
	        TRAP    #15
			MOVE.L  #0,NUMCPLOT             ; VARIABLES CLEARED IN ORDER-
			MOVE.L  #0,NUMCUPDT             ; TO DO THE COUNTING AGAIN
            BRA     .LOOP
            SIMHALT

; --- VARIABLES ---------------------------------------------------------------

            INCLUDE "SYSVAR.X68"
            INCLUDE "VAR.X68"
            END    START









*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
